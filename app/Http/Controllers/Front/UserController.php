<?php

namespace App\Http\Controllers\Front;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Events\UserChanged;

class UserController extends Controller
{
    public function updateUser(Request $request)
    {

      $validator = Validator::make($request->all(), [
        'id' => 'required|integer',
        'name' => 'required|max:191|alpha',
        'email' => 'required|max:191|email',
       ]);

       if ($validator->fails()) {
           return redirect('home')
                       ->withErrors($validator)
                       ->withInput();
       }else{
         $user = User::find($request->id);
         $user->name = $request->name;
         $user->email = $request->email;
         $user->save();
         event(new UserChanged($user));
         return redirect('home')->with('message','User updated, log written.');
       }

    }
}
